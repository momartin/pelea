/************************************************************************
 * Planning and Learning Group PLG,
 * Department of Computer Science,
 * Carlos III de Madrid University, Madrid, Spain
 * http://plg.inf.uc3m.es
 * 
 * Copyright 2012, Mois�s Mart�nez
 *
 * (Questions/bug reports now to be sent to Mois�s Mart�nez)
 *
 * This file is part of Pelea.
 * 
 * Pelea is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Pelea is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Pelea.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ************************************************************************/

package org.pelea.planner.plan;

import java.util.ArrayList;
import java.util.List;

public class PlanAction {
    private String name;
    private List values; 
    private int execution_order;
    private double duration;
    private double cost;
    private double startTime;
    
    public PlanAction(String name, int order)
    {
        this.name = name;
        this.values = new ArrayList<String>();
        this.execution_order = order;
        this.duration = 0;
        this.startTime = 0;
        this.cost = 1;
    }
    
    public PlanAction(String name, int order, double duration)
    {
        this.name = name;
        this.values = new ArrayList<String>();
        this.execution_order = order;
        this.duration = duration;
        this.startTime = 0;
        this.cost = 1;
    }
    
    public PlanAction()
    {
        this.values = new ArrayList<String>();
        this.duration = 0;
        this.cost = 0;
        this.startTime = 0;
    }
    
    public PlanAction(String name, int order, double duration, double cost)
    {
        this.name = name;
        this.values = new ArrayList<String>();
        this.execution_order = order;
        this.duration = duration;
        this.cost = cost;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public void setName(String name)
    {
        this.name = name.toUpperCase();
    }
    
    public String getValue(int position)
    {
        return (String) this.values.get(position);
    }
    
    public void addValue(String value)
    {
        this.values.add(this.values.size(), value.toUpperCase());
    }
    
    public List getValues()
    {
        return this.values;
    }
    
    public double getDuration()
    {
        return this.duration;
    }
    
    public void setDuration(double duration)
    {
        this.duration = duration;
    }
    
    public double getCost()
    {
        return this.cost;
    }
    
    public void setCost(double cost)
    {
        this.cost = cost;
    }
    
    public int size()
    {
        return this.values.size();
    }
    
    public int getExecutionOrder()
    {
        return this.execution_order;
    }
    
    public void setExecutionOrder(int order)
    {
        this.execution_order = order;
    }
    
    public double getStartTime()
    {
        return this.startTime;
    }
    
    public void setStartTime(double time)
    {
        this.startTime = time;
    }
}
