/************************************************************************
 * Planning and Learning Group PLG,
 * Department of Computer Science,
 * Carlos III de Madrid University, Madrid, Spain
 * http://plg.inf.uc3m.es
 * 
 * Copyright 2012, Mois�s Mart�nez
 *
 * (Questions/bug reports now to be sent to Mois�s Mart�nez)
 *
 * This file is part of Pelea.
 * 
 * Pelea is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Pelea is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Pelea.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ************************************************************************/

package org.pelea.planner.plan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.pelea.utils.Util;
import xml2pddl.XML2PDDL;

public class Plan {
    
    private List<PlanAction> actions;
    private HashMap<String, String> variables;
    
    public Plan()
    {
        this.actions = new ArrayList();
        this.variables = new HashMap();
        this.variables.put("time", "0");
        this.variables.put("cost", "0");
    }
    
    public Plan(double time) {
        this.actions = new ArrayList();
        this.variables = new HashMap();
        this.variables.put("time", String.valueOf(time));
        this.variables.put("cost", "0");
    }
    
    public void setTime(double time) {
        this.variables.put("time", String.valueOf(time));
    }
    
    public void setCost(int cost) {
        this.variables.put("cost", String.valueOf(cost));
    }
    
    public void addAction(PlanAction action) {
        this.actions.add(action);
    }
    
    public double getTime() {
        return Double.parseDouble(this.variables.get("time"));
    }
    
    public int getCost() {
        return Integer.parseInt(this.variables.get("cost"));
    }
    
    public void addVariable(String variable, String value) {
        this.variables.put(variable, value);
    }
    
    public PlanAction getAction(int position) {
        return this.actions.get(position);
    }
    
    public String getActionName(int position) {
        return this.actions.get(position).getName();
    }
    
    public String getActionValue(int action, int value) {
        return this.actions.get(action).getValue(value);
    }
    
    public int size() {
        return this.actions.size();
    }
    
    public HashMap getVariables() {
        return this.variables;
    }
    
    private int getNumberOfGroups() {
        return this.actions.get(this.actions.size()-1).getExecutionOrder();
    }
    
    public String generateXML(String domainH, String stateH) {     
        String xmlAction        = "";
        String xmlPlan          = "";
        
        int group               = 0;
        int actionCounter       = 0;        
        
        try 
        {
            //TODO: OTRA PUTA MIERDA MAS DE CESAR, ESTO TENGO QUE CAMBIARLO ES INEFICIENTE A NIVELES CUANTICOS.
            String[] domainActions  = XML2PDDL.getActions(domainH).split("\\s+");
            String[] parameters     = XML2PDDL.getParameters(domainH).split("\\|");
            String[] params_action;
            
            xmlPlan += "<plan";
            
            for (Map.Entry element : this.variables.entrySet()) {
                xmlPlan += " " + element.getKey() + "=\"" + element.getValue() + "\"";
            }
            
            xmlPlan += ">";
            
            for (int i = 0; i < actions.size(); i++)
            {
                PlanAction action = this.actions.get(i);
             
                if (action.getExecutionOrder() != group)
                {
                    xmlPlan += "<action-plan num_actions=\"" + actionCounter + "\">";
                    xmlPlan += xmlAction;
                    xmlPlan += "</action-plan>";
                    
                    xmlAction = "";
                    actionCounter = 0;
                    
                    group++;
                }
                
                xmlAction += "<action name=\"" + action.getName() + "\" start_time=\"" + action.getStartTime() + "\" execution_time=\"" + action.getCost() + "\">";
                
                params_action = parameters[Util.getPosition(domainActions, action.getName())].split("\\s+");
               
                for (int j = 0; j < action.size(); j++)
                {
                   xmlAction += "<term name=\"" + action.getValue(j).toUpperCase() + "\" class=\"" + params_action[j].toUpperCase() + "\" type=\"pddl_variable\"/>";      
                }

                xmlAction += "</action>";
                
                actionCounter++;
            }
            
            xmlPlan += "<action-plan num_actions=\"" + actionCounter + "\">";
            xmlPlan += xmlAction;
            xmlPlan += "</action-plan>";

            xmlPlan += "</plan>";

        } 
        catch (Exception ex) 
        {
            xmlPlan = "<error>";
            xmlPlan += "<type>8</type>";
            xmlPlan += "<message>Error processing plan</message>";
            xmlPlan += "</error>";
        }
        
        return xmlPlan;

    }
}
