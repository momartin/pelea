/************************************************************************
 * Planning and Learning Group PLG,
 * Department of Computer Science,
 * Carlos III de Madrid University, Madrid, Spain
 * http://plg.inf.uc3m.es
 * 
 * Copyright 2012, Mois�s Mart�nez
 *
 * (Questions/bug reports now to be sent to Mois�s Mart�nez)
 *
 * This file is part of Pelea.
 * 
 * Pelea is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Pelea is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Pelea.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ************************************************************************/

package org.pelea.planners;

import java.io.File;
import java.util.Iterator;
import javaff.JavaFF;
import javaff.data.TotalOrderPlan;
import org.pelea.core.configuration.configuration;
import org.pelea.planner.Planner;
import org.pelea.utils.Util;
import xml2pddl.XML2PDDL;

public class JFF extends Planner {

    public JFF(String id) {
        super(id, "JFF", false);
    }

    @Override
    public String getRePlanH(String domain, String problem, String plan) throws Exception {
        return this.getPlanH(domain, problem);
    }

    @Override
    public String getComand(String domain, String problem, int type) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComand(String domain, String problem, String plan, int type) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String getPlanH(String domainH, String problemH) throws Exception {
        
        long start_time, end_time;
        String result = "";
        
        String domain_path  = this.temp + "domain.pddl";
        String problem_path = this.temp + "problem.pddl";
        
        try {
            
            if ((configuration.getInstance().getParameter("GENERAL", "TEMPORAL") != null) && (configuration.getInstance().getParameter("GENERAL", "TEMPORAL").equals("YES"))) {
                this.createFile(XML2PDDL.convertDomainTemporal(domainH), domain_path);
                this.createFile(XML2PDDL.convertProblemTemporal(problemH), problem_path);
            }
            else {
                this.createFile(XML2PDDL.convertDomain(domainH), domain_path);
                this.createFile(XML2PDDL.convertProblem(problemH), problem_path);
            }
            
            Util.printDebug(this.getName(), "Generating temporal files by domain and problem in PDDL");
        } 
        catch (Exception e) {
            result = Util.generateMessageError("3", "Domain or problem could not be converted into PDDL");  
            return result;
        }
        
        start_time = System.currentTimeMillis();
        TotalOrderPlan plan = (TotalOrderPlan) JavaFF.plan(new File(domain_path), new File(problem_path));
        end_time = System.currentTimeMillis();
            
        this.time = end_time - start_time;
        
        if (plan != null) {
        
            Iterator actions = plan.iterator();
            
            result += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            result += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            result += "<plans name=\"xPddlPlan\" domain=\" " + XML2PDDL.getNameDomain(domainH) + "\">";
            result += "<plan time=\"" + this.time + "\" cost=\"" + plan.getPlanLength()  + "\">";
            
            while (actions.hasNext()) {
                String action[] = actions.next().toString().split(" ");
                
                result += "<action-plan num_actions=\"1\">";
                result += "<action name=\"" + action[0] + "\" start_time=\"1\" execution_time=\"1\">";
                
                for (int i = 1; i < action.length; i++) {
                   result += "<term name=\"" + action[i].toUpperCase() + "\" type=\"pddl_variable\"/>";      
                }

                result += "</action>";
                result += "</action-plan>";
            }
            
            result += "</plan>";
            result += "</plans>";
            result += "</define>";
        }
        else {
            result += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            result += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            result += "<plans name=\"xPddlPlan\" domain=\" " + XML2PDDL.getNameDomain(domainH) + "\">";
            result += "<plan time=\"" + this.time + "\"/>";
            result += "</plans>";
            result += "</define>";
        }
        
        return result;
    }
    
}
